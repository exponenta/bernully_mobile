﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GradientGraphics : MaskableGraphic
{

	public Gradient Gradient;
	[Range(1,20)]
	public int StepsCount = 12;
	[Range(0,1f)]
	[SerializeField] private float _fill = 1f;

	public float Fill
	{
		get { return _fill; }
		set
		{
			_fill = value;
			base.SetLayoutDirty();
		}
	}

	protected override void OnPopulateMesh(VertexHelper vh)
	{	
		vh.Clear();

		var pixelAdjustedRect = this.GetPixelAdjustedRect();
		var vector4 = new Vector4(pixelAdjustedRect.x,
			pixelAdjustedRect.y,
			pixelAdjustedRect.x + pixelAdjustedRect.width,
			pixelAdjustedRect.y + pixelAdjustedRect.height);

		var size = Mathf.CeilToInt(StepsCount * _fill);
		var step = pixelAdjustedRect.width / StepsCount;

		var pice = pixelAdjustedRect.width * ((float)size - StepsCount * _fill) / (float)StepsCount ;
		
		for (var index = 0; index < size; index++)
		{
			var col = Gradient.Evaluate((float)index / StepsCount);
			col *= color;
			
			var offset = step * index;
			if (index == size - 1)
			{
				step -=pice;
			}

			vh.AddVert(new Vector3(vector4.x + offset, vector4.y), col, new Vector2(0.0f, 0.0f));
			vh.AddVert(new Vector3(vector4.x + offset, vector4.w), col, new Vector2(0.0f, 1f));
			vh.AddVert(new Vector3(vector4.x + step + offset, vector4.w), col, new Vector2(1f, 1f));
			vh.AddVert(new Vector3(vector4.x + step + offset, vector4.y), col, new Vector2(1f, 0.0f));

			vh.AddTriangle(0 + index * 4, 1 + index * 4, 2 + index * 4);
			vh.AddTriangle(2 + index * 4, 3 +  index * 4, 0 +  index * 4);
		}
		
	}
	

}
