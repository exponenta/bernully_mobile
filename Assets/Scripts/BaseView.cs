using System;
using UnityEngine;

public interface IModel
{
}

public class BaseView : MonoBehaviour
{
    public IModel LatestModel { get; protected set; }
    
    public virtual void BindModel(IModel model)
    {
        LatestModel = model;
    }
}
