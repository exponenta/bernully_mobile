﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class RangeItemView : BaseView
{
    [System.Serializable]
    public class RangeItemModel : IModel
    {
        public int Offset;
        public float FillAmount;
        public float Odds;
        public float Odds10;
        public bool IsActive;
    }

    public TMP_Text Offset_Text;
    public TMP_Text Odds_Text;
    public TMP_Text Odds10_Text;
    public GradientGraphics Gradient;
    public CanvasGroup Group;

    public Gradient OddsGradient;
    public float OddsFlashDuration = 0.5f;

    private Coroutine _flash;
    [Space(10)] [SerializeField] public RangeItemModel BaseModel;

    private void Awake()
    {
        BindModel(BaseModel);
    }

    public override void BindModel(IModel model)
    {
        var m = model as RangeItemModel;
        if (m == null)
            return;
        Group.alpha = m.IsActive ? 1f : 0.15f;
        Odds_Text.color = m.IsActive ? OddsGradient.Evaluate(m.Odds) : Color.gray;

        var l = LatestModel as RangeItemModel;
        if (m.IsActive || (l != null && l.IsActive))
        {
            Offset_Text.text = m.Offset > 0 ? $"+{m.Offset}" : $"{m.Offset}";
            Gradient.Fill = m.FillAmount;
            Odds_Text.text = m.Odds.ToString("P");
            Odds10_Text.text = m.Odds10.ToString("P");
        }


        if (m.Odds > 0.92f && m.IsActive)
        {
            _flash = StartCoroutine(Flash());
        }
        else
        {
            if (_flash != null)
            {
                StopCoroutine(_flash);
                _flash = null;
            }
        }

        base.BindModel(m);
    }

    public IEnumerator Flash()
    {
        var startColor = Odds_Text.color;
        var endColor = startColor;
        endColor.a = 0;

        var forward = true;
        var lerp = 0.0f;

        while (true)
        {
            lerp += Time.deltaTime / OddsFlashDuration * (forward ? 1f : -1f);
            if (lerp >= 1f || lerp < 0)
            {
                forward = !forward;
                lerp = Mathf.Clamp01(lerp);
            }

            Odds_Text.color = Color.Lerp(startColor, endColor, lerp);
            yield return null;
        }
    }
}