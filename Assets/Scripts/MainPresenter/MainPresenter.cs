﻿using System.Collections.Generic;
using System.Linq;
using Bernully.Settings;
using PockerHelper;
using PockerHelper.Core;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class MainPresenter : BasePresenter
{
    public AppController App;
    public Button AddButton;
    public Transform RangeItemParent;
    private List<RangeItemView> _items;

    private MainPresenterModel _mainModel;

    public List<RangeItemView> Items
    {
        get
        {
            if (_items == null || _items.Count == 0)
            {
                _items = new List<RangeItemView>();
                foreach (Transform t in RangeItemParent)
                    _items.Add(t.GetComponent<RangeItemView>());
            }

            return _items;
        }
    }

    public class MainPresenterModel : IModel
    {
        public List<Calculator> Calculators;
        public SettingsModel Settings;
    }

    private RangeItemView.RangeItemModel CreateModel(ComputedEntry model, int index)
    {
        var lineData = new LineData();
        lineData.LastUpdateEntry = model;

        return new RangeItemView.RangeItemModel()
        {
            FillAmount = lineData.MappedSob / 100f,
            Odds = lineData.Odds / 100f,
            Odds10 = lineData.Odds10 / 100f,
            IsActive = _mainModel.Settings.Ranges[index].IsActive,
            Offset = (int) lineData.Delta
        };
    }

    public override void Init(IModel model)
    {
        _mainModel = model as MainPresenterModel;

        AddButton.onClick.AsObservable().Subscribe(x => { App.AddCalcData("Jh Ac"); });

        if (_mainModel != null)
        {
            for (var i = 0; i < _mainModel.Calculators.Count; i++)
            {
                var rangeV = Items[i];

                rangeV.BindModel(CreateModel(null, i));

                var index = i;

                _mainModel.Calculators[i].ComputedEntries.ObserveAdd().Subscribe(
                    cent => { rangeV.BindModel(CreateModel(cent.Value, index)); });
            }

            _mainModel.Settings.OnPropertyChanged += (n, v) =>
            {
                for (var i = 0; i < Items.Count; ++i)
                {
                    var calc = _mainModel.Calculators[i];
                    var latest = calc.ComputedEntries.LastOrDefault();

                    Items[i].BindModel(CreateModel(latest, i));
                }
            };
        }
    }
}