﻿using System;
using PockerHelper.Core;
using UniRx;

namespace PockerHelper
{
    public sealed class LineData
    {
        private const float START_VALUE = 100f / 12f;

        public Calculator Calc { get; set; }
        public string Name { get; set; }
        public float Value { get; set; }

        private ComputedEntry _lastComputedEntry;

        public ComputedEntry LastUpdateEntry
        {
            set
            {
                _lastComputedEntry = value;
                MappedSob = CreateMappedSob();
            }
            get { return _lastComputedEntry; }
        }


        public float Odds
        {
            get
            {
                //return 98f;

                return LastUpdateEntry != null ? LastUpdateEntry.Probability : 0f;
            }
        }

        public float Odds10
        {
            get { return LastUpdateEntry != null ? LastUpdateEntry.Probability10 : 0f; }
        }

        public float CreateMappedSob()
        {
            if (_lastComputedEntry == null)
                return START_VALUE;

            var sob = _lastComputedEntry.SobCount;

            if (sob < _lastComputedEntry.Min)
                return START_VALUE;
            if (sob >= _lastComputedEntry.Max)
                return 120f;

            var avg = _lastComputedEntry.Avr;
            var max = _lastComputedEntry.Max;

            float res = 0;
            if (sob < avg)
            {
                res = (sob * 50.0f) / avg;
            }
            else
            {
                res = 50 * (1 + (sob - avg) / (max - avg));
            }

            return Math.Max(0, res) * (1f - 2f / 12) + START_VALUE;
        }

        public float MappedSob { get; set; } = START_VALUE;

        public float Delta
        {
            get
            {
                if (LastUpdateEntry == null) return 0;
                return (float) Math.Truncate(LastUpdateEntry.SobCount - LastUpdateEntry.Avr);
            }
        }

    }
}