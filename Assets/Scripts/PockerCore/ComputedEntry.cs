﻿using System;

namespace PockerHelper.Core
{
    public class ComputedEntry
    {
        public int Number { get; set; }
        public int Counter { get; set; }
        public int SobCount { get; set; }

        public string Cards => HandData.CardsSet;

        public long HandId => HandData.Id;

        public bool IsEnter => Weight * 0.01f <= Range;

        public ComputeModes Mode { get; set; } = 0;

        public float Range { get; set; }

        public PockerstarsBackend.PockerHandsData HandData { get; set; }

        public float Weight => HandData.MappedData[Mode];

        public float Avr => Number * Range;

        public float Dispersion => (1.0f - Range) * Avr;

        public float Min => (float) Math.Max(0, Avr - 3.0f * Math.Sqrt(Dispersion));

        public float Max => Avr + 3.0f * (float) Math.Sqrt(Dispersion);

        public float Probability => 100.0f * (1.0f - (float) Math.Pow(1.0f - Range, Counter));

        public float Probability10 => 100.0f * (1.0f - (float) Math.Pow(1.0f - Range, Counter + 10));

        internal void Reset()
        {
            HandData = null;
            Number = 0;
            Counter = 0;
            SobCount = 0;
            Mode = 0;
            Range = 0;
        }
    }
}