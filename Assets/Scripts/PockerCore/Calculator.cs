﻿using System;
using PockerstarsBackend;
using UniRx;

namespace PockerHelper.Core
{
    public class Calculator
    {
        public object _locker = new object();

        public ReactiveCollection<ComputedEntry> ComputedEntries { get; } = new ReactiveCollection<ComputedEntry>();

        public ReactiveCollection<PockerHandsData> HandsDatas { get; }

        private ComputeModes _mode = ComputeModes.Nolimit;

        public ComputeModes ComputeMode
        {
            get { return _mode; }
            set
            {
                var last = _mode;
                _mode = value;
                if (last != _mode)
                {
                    Rebuild();
                }
            }
        }

        private float _range = 0;

        public float Range
        {
            get { return _range; }
            set
            {
                var last = _range;
                _range = value;
                if (Math.Abs(_range - last) > (2.0f / 1326))
                {
                    Rebuild();
                }
            }
        }

        public bool Updating { get; set; }

        public int Id { get; set; } = 0;

        private int _probabilityCounter = 0;
        private int _incrementalCounter = 0;

        public Calculator(float startRange, ComputeModes startMode, ReactiveCollection<PockerHandsData> _hands)
        {
            _range = startRange;
            _mode = startMode;
            HandsDatas = _hands;
            HandsDatas.ObserveAdd().Subscribe(x =>
            {
                var item = CreateItem(x.Value, ComputedEntries.Count);
                ComputedEntries.Add(item);
            });
        }

        public void Rebuild()
        {
            if (!Updating)
                return;


            _probabilityCounter = 0;
            _incrementalCounter = 0;

            //_loger.Info($"[{Id}] Full Rebuild");

            try
            {
                //ComputedEntries. = true;
                ComputedEntries.Clear();

                var count = HandsDatas.Count;
                for (var i = count - 1; i >= 0; i--)
                {
                    var reverse = count - i - 1;
                    ComputedEntries[i] = CreateItem(HandsDatas[reverse], reverse, ComputedEntries[i]);
                }

                //_loger.Info($"[{Id}] Items count {ComputedEntries.Count}");
            }
            catch (Exception e)
            {
                //_loger.Error(e);
                //_loger.Warn($"[{Id}] Clear entries because data was corrupted");

                ComputedEntries.Clear();
            }
        }


        private ComputedEntry CreateItem(PockerstarsBackend.PockerHandsData handsData, int index,
            ComputedEntry computed = null)
        {
            //var compute = new ComputedEntry()
            //{
            if (computed == null)
                computed = new ComputedEntry();

            computed.Reset();

            computed.Mode = ComputeMode;
            computed.Range = Range;
            computed.HandData = handsData;
            computed.Number = index + 1;
            //};

            if (!computed.IsEnter)
            {
                _probabilityCounter++;
            }
            else
            {
                _incrementalCounter++;
                _probabilityCounter = 0;
            }

            computed.Counter = _probabilityCounter;
            computed.SobCount = _incrementalCounter;

            return computed;
        }
    }
}