﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace PockerHelper.Core
{
    public class RangeMapper
    {
        Dictionary<string, float> _range = new Dictionary<string, float>();
        public RangeMapper(string mappingData)
        {
            var lines = mappingData.Split('\n');

            foreach (var item in lines)
            {
                if (string.IsNullOrWhiteSpace(item))
                    continue;

                var pairs = item.Split(' ');

                var key = pairs[0].Trim();
                var value = float.Parse(pairs[1].Trim(), NumberStyles.Any, CultureInfo.InvariantCulture);
                _range.Add(key, value);
            }
        }

        public float Map(string[] cards)
        {
            cards[0] = cards[0].Trim();
            cards[1] = cards[1].Trim();

            var m1 = cards[0].Substring(0, 1);
            var m2 = cards[1].Substring(0, 1);

            var s1 = cards[0].Substring(1, 1);
            var s2 = cards[1].Substring(1, 1);

            var isPair = m1.Equals(m2, StringComparison.OrdinalIgnoreCase);
            var isSuitsEqual = s1.Equals(s2, StringComparison.OrdinalIgnoreCase);

            var suff = "p";
            if (!isPair)
            {
                suff = isSuitsEqual ? "s" : "o";
            }

            var key1 = m1.ToUpper() + m2.ToUpper() + suff;
            var key2 = m2.ToUpper() + m1.ToUpper() + suff;

            if (_range.ContainsKey(key1))
                return _range[key1];

            if (_range.ContainsKey(key2))
                return _range[key2];

            return -1;
        }
    }
}
