﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using PockerHelper;
using PockerHelper.Core;

namespace PockerstarsBackend
{
    public class PockerHandsData : IEquatable<PockerHandsData>
    {
        public long Id { get; set; }
        public DateTime Time { get; set; }
        private string[] Cards { get; set; }

        public string CardsSet => string.Join(" ", Cards);

        private Dictionary<ComputeModes, float> _mappedData;

        public Dictionary<ComputeModes, float> MappedData
        {
            get
            {
                if (_mappers == null)
                    return null;

                if (_mappedData == null)
                {
                    _mappedData = new Dictionary<ComputeModes, float>();

                    foreach (var pair in _mappers.Mappers)
                    {
                        _mappedData.Add(pair.Key, pair.Value.Map(Cards));
                    }
                }

                return _mappedData;
            }
        }

        private RangeDataMapper _mappers;

        public PockerHandsData(RangeDataMapper mappers, string cards)
        {
            _mappers = mappers;
            Cards = cards.Split(' ');
        }

        public override string ToString()
        {
            var mappeds = "";
            if (MappedData != null)
            {
                mappeds = string.Join(", ", MappedData.Select(x => x.ToString()));
            }

            return
                $@"{Id}, {string.Join(" ", Cards)}, {Time.ToShortDateString()} {Time.ToShortTimeString()}, {mappeds}";
        }

        public bool Equals(PockerHandsData other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Id == other.Id;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((PockerHandsData) obj);
        }

        public override int GetHashCode()
        {
            int hashCode = Id.GetHashCode();
            return hashCode;
        }
    }
}