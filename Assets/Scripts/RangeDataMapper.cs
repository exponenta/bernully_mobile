﻿using System.Collections;
using System.Collections.Generic;
using PockerHelper;
using UnityEngine;
using PockerHelper.Core;

[CreateAssetMenu(fileName = "RangeMapper", menuName = "Range Mapper")]
public class RangeDataMapper : ScriptableObject
{
    [SerializeField] TextAsset _noLimit, _pockerstove, _sklanskyChubuk, _sklanskyMalm, _hu;

    private Dictionary<ComputeModes, RangeMapper> _mapper;
    public Dictionary<ComputeModes, RangeMapper> Mappers
    {
        get
        {
            if(_mapper == null)
                BuildMapper();

            return _mapper;
        }
    }

    public void BuildMapper()
    {
        _mapper = new Dictionary<ComputeModes, RangeMapper>()
        {
            {ComputeModes.Nolimit, new RangeMapper(_noLimit.text)},
            {ComputeModes.Pockerstove, new RangeMapper(_pockerstove.text)},
            {ComputeModes.SklanskyChubuk, new RangeMapper(_sklanskyChubuk.text)},
            {ComputeModes.SklanskyMalm, new RangeMapper(_sklanskyMalm.text)},
            {ComputeModes.HU, new RangeMapper(_hu.text)}
        };
    }
}
