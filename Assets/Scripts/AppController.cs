﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Bernully.Settings;
using UnityEngine;
using PockerHelper.Core;
using PockerstarsBackend;
using UniRx;

public class AppController : MonoBehaviour
{
    [SerializeField] private RangeDataMapper _mapper;
    public RangeDataMapper Mapper => _mapper;

    [Space(10)] [SerializeField] SettingsModel _settingsModel = new SettingsModel();
    [SerializeField] SettingsPresenter _settingsPresenter;

    [Space(10)] [SerializeField] private MainPresenter _mainPresenter;

    private List<Calculator> _calculators;
    private ReactiveCollection<PockerHandsData> _hands = new ReactiveCollection<PockerHandsData>();

    private void Awake()
    {
        Screen.fullScreen = false;

        LoadSettings();

        _settingsPresenter.Init(_settingsModel);
        _settingsModel.OnPropertyChanged += (n, v) => SaveSettings();

        _calculators = Enumerable.Range(0, _settingsModel.Ranges.Count)
            .Select(
                x => new Calculator(
                    _settingsModel.Ranges[x].NormalValue,
                    _settingsModel.RangeMode, _hands))
            .ToList();

        _settingsModel.OnPropertyChanged += (n, value) =>
        {
            for (var i = 0; i < _calculators.Count; i++)
            {
                _calculators[i].ComputeMode = _settingsModel.RangeMode;
                _calculators[i].Range = _settingsModel.Ranges[i].NormalValue;
            }
        };
    }

    private void Start()
    {
        _calculators.ForEach(x =>
        {
            x.Rebuild();
            x.ComputedEntries.ObserveAdd().Subscribe(add =>
            {
                Debug.Log("Calc weight:" + add.Value.Weight + " value:" + add.Value.Avr);
            });
        });

        _mainPresenter.Init(new MainPresenter.MainPresenterModel()
        {
            Calculators = _calculators,
            Settings = _settingsModel
        });
    }

    public void AddCalcData(string cards)
    {
        var hands = new PockerHandsData(_mapper, cards);
        _hands.Add(hands);
    }

    private void LoadSettings()
    {
        var json = PlayerPrefs.GetString("_settings_");
        Debug.Log(json);
        if (!string.IsNullOrEmpty(json))
            JsonUtility.FromJsonOverwrite(json, _settingsModel);
    }

    private void SaveSettings()
    {
        var json = JsonUtility.ToJson(_settingsModel, false);
        PlayerPrefs.SetString("_settings_", json);
        PlayerPrefs.Save();
    }
}