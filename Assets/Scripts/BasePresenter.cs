﻿using UnityEngine;

public abstract class BasePresenter: MonoBehaviour
{
	public abstract void Init(IModel model);
}