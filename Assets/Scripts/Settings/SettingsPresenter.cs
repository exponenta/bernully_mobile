﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Bernully.Settings;
using Bernully.Settings.UI;
using PockerHelper;
using TMPro;
using UnityEngine;

public class SettingsPresenter : BasePresenter
{
    private SettingsModel _model;

    public SettingsModel SettingsModel
    {
        get { return _model; }
        set
        {
            _model = value;
            RefreshUi();
        }
    }

    [SerializeField] private RangeSettingsItemView[] _rangeViews;
    [SerializeField] private TMP_Dropdown _rangeMode;

    private bool _isInit = false;
    private Canvas _canvas;

    public override void Init(IModel model)
    {
        SettingsModel = model as SettingsModel;

        _canvas = GetComponent<Canvas>();

        var names = Enum.GetNames(typeof(ComputeModes));
        _rangeMode.options = new List<TMP_Dropdown.OptionData>(names.Select(x => new TMP_Dropdown.OptionData(x)));
        _rangeMode.onValueChanged.AddListener((x) => { SettingsModel.RangeMode = (ComputeModes) x; });

        Hide();
        _isInit = true;
    }

    public void RefreshUi()
    {
        if (!_isInit || !_canvas.enabled)
            return;

        if (SettingsModel == null)
            return;

        var index = 0;
        foreach (var rv in _rangeViews)
        {
            var indx = index++;
            var bindable = SettingsModel.Ranges.Count > indx ? SettingsModel.Ranges[indx] : null;

            rv.BindModel(bindable);

            bindable.OnPropertyChanged += (name, value) =>
            {
                if (name == "IsActive" && _rangeViews.All(x => !x.LatestRangeModel.IsActive))
                {
                    var model = _rangeViews[0].LatestRangeModel;
                    model.IsActive = true;
                    _rangeViews[0].BindModel(model);
                }
            };
        }

        _rangeMode.value = (int) SettingsModel.RangeMode;
    }

    public void Show()
    {
        _canvas.enabled = true;
        RefreshUi();
    }

    public void Hide()
    {
        _canvas.enabled = false;
        SettingsModel.Change();
    }
}