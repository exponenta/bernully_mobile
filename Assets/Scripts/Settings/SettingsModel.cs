using System;
using System.Collections.Generic;
using Bernully.Settings.UI;
using PockerHelper;
using UniRx;
using UnityEngine;

namespace Bernully.Settings
{
    [Serializable]
    public class SettingsModel : IModel
    {
        public List<RangeSettingsItemView.RangeSettingModel> Ranges;
        public ComputeModes RangeMode = ComputeModes.Nolimit;

        public event Action<string, object> OnPropertyChanged;

        public void Change(string name = null, object val = null)
        {
            OnPropertyChanged?.Invoke(name, val);
        }
    }
}