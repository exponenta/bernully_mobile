﻿using System;
using System.Globalization;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Bernully.Settings.UI
{
    public class RangeSettingsItemView : BaseView
    {
        [Serializable]
        public class RangeSettingModel : IModel
        {
            public readonly int MaxValue = 1366;
            public readonly int MinValue = 2;

            public bool IsActive;
            public int Value;

            public float NormalValue
            {
                get { return Value / (float) MaxValue; }
                set
                {
                    var v = Mathf.Clamp01(value);
                    Value = (int) (v * MaxValue);
                }
            }

            public event Action<string, object> OnPropertyChanged;

            public void Change(string name = null, object val = null)
            {
                OnPropertyChanged?.Invoke(name, val);
            }
        }

        [SerializeField] TMP_InputField ValueInputField;
        [SerializeField] TMP_InputField ValuePercentageField;
        [SerializeField] Slider ValueSlider;
        [SerializeField] Toggle IsActiveToggle;
        [SerializeField] CanvasGroup CanvasGroup;

        [SerializeField] RangeSettingModel _baseModel;

        public RangeSettingModel LatestRangeModel
        {
            get { return (RangeSettingModel) LatestModel; }
            protected set { LatestModel = value; }
        }

        void Awake()
        {
            BindModel(_baseModel);
        }

        public override void BindModel(IModel model)
        {
            LatestModel = model ?? new RangeSettingModel() {IsActive = false};
            SyncUi();
        }

        private void BindEvents()
        {
            ValueSlider.onValueChanged.AddListener((value) =>
            {
                LatestRangeModel.Value = (int) value;
                LatestRangeModel.Change("Value", LatestRangeModel.Value);

                ValueInputField.text = LatestRangeModel.Value.ToString("D");
                ValuePercentageField.text = LatestRangeModel.NormalValue.ToString("P1");
            });

            ValueInputField.onEndEdit.AddListener((value) =>
            {
                int intVal = LatestRangeModel.Value;

                if (int.TryParse(value, out intVal) && intVal > LatestRangeModel.MinValue &&
                    intVal < LatestRangeModel.MaxValue)
                {
                    LatestRangeModel.Value = intVal;
                    LatestRangeModel.Change("Value", LatestRangeModel.Value);
                }

                SyncUi();
            });

            ValuePercentageField.onEndEdit.AddListener((value) =>
            {
                float floatVal = LatestRangeModel.NormalValue;
                value = value.Replace("%", "");

                if (float.TryParse(value, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out floatVal) &&
                    floatVal < 100f && floatVal > 0f)
                {
                    LatestRangeModel.NormalValue = floatVal / 100f;
                    LatestRangeModel.Change("NormalValue", LatestRangeModel.NormalValue);
                }

                SyncUi();
            });

            IsActiveToggle.onValueChanged.AddListener((value) =>
            {
                LatestRangeModel.IsActive = value;
                LatestRangeModel.Change("IsActive", LatestRangeModel.IsActive);

                SyncUi();
            });
        }

        private void ReleaseEvents()
        {
            ValueInputField.onEndEdit.RemoveAllListeners();
            ValuePercentageField.onEndEdit.RemoveAllListeners();
            ValueSlider.onValueChanged.RemoveAllListeners();
            IsActiveToggle.onValueChanged.RemoveAllListeners();
        }

        private void SyncUi()
        {
            ReleaseEvents();

            CanvasGroup.interactable = LatestRangeModel.IsActive;
            ValueSlider.value = LatestRangeModel.Value;
            ValueInputField.text = LatestRangeModel.Value.ToString("D");
            ValuePercentageField.text = LatestRangeModel.NormalValue.ToString("P1");
            IsActiveToggle.isOn = LatestRangeModel.IsActive;

            BindEvents();
        }
    }
}